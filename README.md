# team-leap-rfid-api

## Prerequisites

- Any Raspberry Pi
- Python3 Module
    - MFRC522/SimpleMFRC522
    - RPi.GPIO

## Configuartion
```json
#config.json
{
    "server_url": "example.com",
    "board_pins": {
      "kontext": 5,
      "implementation": 6,
      "addon": 13
    },
    "button_pin" : 17,
    "key_pin" : 22,
    "status_pin" : 27
}
```

`server_url` is your backend endpoint. We are using the `GPIO.BCM` option, which means that you are referring to the pins by the "Broadcom SOC channel" number. For example, `GPIO.17, GPIO.22, etc`. In `board_pins`, you will specify the pins that you want to use for the RFID reader. `button_pin` is the launch button. `key_pin` is the lock mechanism. `status_pin` is the pin that signals the presence of internet connectivity.