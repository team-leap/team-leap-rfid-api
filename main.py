#!/usr/bin/env python

import RPi.GPIO as GPIO
from mfrc522 import SimpleMFRC522
import requests
import datetime
import json
import NFC
import threading
import time

def check_change(old, new):

    if old == None and new != None:
        send_insert(new)
    elif old != None and new == None:
        send_remove(old)
    elif old != None and new != None and old != new:
        log("third")
        send_remove(old)
        send_insert(new)
    else:
        return


def send_insert(block):
    try:
        response = requests.get(server_url + "/action/block/insert/" + str(block))
        response.raise_for_status()
        log("insert request for " + str(block) + " sended")
    except requests.exceptions.RequestException as e:
        log("error: " + str(e))

def send_remove(block):
    try:
        response = requests.get(server_url + "/action/block/remove/" + str(block))
        response.raise_for_status()
        log("remove reqeust for " + str(block) + " sended")
    except requests.exceptions.RequestException as e:
        log("error: " + str(e))

def send_button(channel):
    try:
        response = requests.get(server_url + "/action/button")
        response.raise_for_status()
        print("button pressed")
        log("button request sended")
    except requests.exceptions.RequestException as e:
        log("error: " + str(e))


def send_keyflick(channel):
    try:
        response = requests.get(server_url + "/action/key")
        response.raise_for_status()
        print("key flicked")
        log("key request sent")
    except requests.exceptions.RequestException as e:
        log("error: " + str(e))

def log(action):
    with open("/home/leap/git/team-leap-rfid-api/log.txt", "a") as file:
        current_time = datetime.datetime.now()
        formated_time = current_time.strftime("%Y-%m-%d %H:%M:%S")
        file.write(formated_time + " - " + action + "\n")

def send_heartbeat():
    while True:
        try:
            response = requests.get(server_url + "/device/heartbeat/raspberry_pi")
            response.raise_for_status()
            GPIO.output(data["status_pin"], GPIO.HIGH)
            log("send heartbeat")
        except:
            GPIO.output(data["status_pin"], GPIO.LOW)
            log("error: in send_heartbeat")
        time.sleep(5)

if __name__ == "__main__":

    GPIO.setmode(GPIO.BCM)
    GPIO.setwarnings(False)

    reader = NFC.NFC()

    with open("/home/leap/git/team-leap-rfid-api/config.json") as file:
        data = json.load(file)

    GPIO.setup(data["button_pin"], GPIO.IN, pull_up_down=GPIO.PUD_UP)
    GPIO.add_event_detect(data["button_pin"], GPIO.FALLING, callback=send_button, bouncetime=2000)

    GPIO.setup(data["key_pin"], GPIO.IN, pull_up_down=GPIO.PUD_UP)
    GPIO.add_event_detect(data["key_pin"], GPIO.FALLING, callback=send_keyflick, bouncetime=2000)

    GPIO.setup(data["status_pin"], GPIO.OUT)

    server_url = data["server_url"]
    board_pins = data["board_pins"]

    block_list = {}

    for key, value in board_pins.items():
        reader.addBoard(key, value)
        block_list[key] = None
        GPIO.setup(value, GPIO.OUT)

    try:
        heartbeat = threading.Thread(target=send_heartbeat)
        heartbeat.start()
        while True:
            old_block_list = block_list.copy()
            print(block_list)
            for key in board_pins.keys():
                block_list[key] = reader.read(key)
                check_change(old_block_list[key], block_list[key])
    finally:
        GPIO.cleanup()